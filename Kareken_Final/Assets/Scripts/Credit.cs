﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Credit : MonoBehaviour
{
    public Text myText;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        myText.text = "Credits: DigitalSin: Low poly Office, niko-3d-blends: Low Poly Futuristic City and Futuristic Robot, Delthor Games: Low Poly Sci-Fi Corridors, FireBoltStudios: Oxar Light Freighter, freeCodeCamp.org: tutorial for Unity FPS game, Winterbyte: Assets from FPS tutorial";
    }
}
