﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerResize : MonoBehaviour
{
    public static float currentSize = 1f;

    private GameObject player;
    private Vector3 playerScale;

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");

        Health.onEnemyKill += Grow;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Q) && currentSize > 0.5f)
        {
            Shrink();
        }
    }

    private void OnDisable()
    {
        Health.onEnemyKill -= Grow;
    }

    void Grow()
    {
        playerScale = player.transform.localScale;
        player.transform.localScale = new Vector3(1, 1, 1);

        currentSize = 1f;
    }

    void Shrink()
    {
        playerScale = player.transform.localScale;
        player.transform.localScale = new Vector3(playerScale.x - 0.2f, playerScale.y - 0.2f, playerScale.z - 0.2f);

        currentSize -= 0.2f;
    }
}
