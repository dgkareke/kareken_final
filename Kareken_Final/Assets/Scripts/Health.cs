﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Health : MonoBehaviour
{
    public delegate void Resize();
    public static event Resize onEnemyKill;

    private EnemyAnimator enemyAnim;
    private NavMeshAgent navAgent;
    private EnemyController enemyController;

    public float health = 100f;

    public bool isPlayer, isEnemy;

    private bool isDead;

    //private EnemyAudio enemyAudio;
   
    void Awake()
    {

        if (isEnemy)
        {
            enemyAnim = GetComponent<EnemyAnimator>();
            enemyController = GetComponent<EnemyController>();
            navAgent = GetComponent<NavMeshAgent>();
            //enemyAudio = GetComponentInChildren<EnemyAudio>();
        }

    }

    public void ApplyDamage(float damage)
    {
        // if we died don't execute the rest of the code
        if (isDead)
            return;

        health -= damage;

        //print for testing
        print("Damage dealt hit: " + damage + "Target's current health: " + health);

        if (isEnemy)
        {
            if (enemyController.Enemy_State == EnemyState.PATROL)
            {
                enemyController.chaseDistance = 50f;
            }
        }

        if (health <= 0f)
        {

            PlayerDied();

            isDead = true;
        }
    }

    void PlayerDied()
    {

        if (isEnemy)
        {

            navAgent.velocity = Vector3.zero;
            navAgent.isStopped = true;
            enemyController.enabled = false;

            //enemyAnim.Dead();

            StartCoroutine(DeadSound());

            // EnemyManager spawn more enemies
            //EnemyManager.instance.EnemyDied(false);
        }

        if (isPlayer)
        {

            GameObject[] enemies = GameObject.FindGameObjectsWithTag(Tags.ENEMY_TAG);

            for (int i = 0; i < enemies.Length; i++)
            {
                enemies[i].GetComponent<EnemyController>().enabled = false;
            }

            // call enemy manager to stop spawning enemies
            //EnemyManager.instance.StopSpawning();

            GetComponent<PlayerMovement>().enabled = false;
            GetComponent<PlayerShoot>().enabled = false;
            GetComponent<WeaponManager>().GetCurrentSelectedWeapon().gameObject.SetActive(false);

        }

        if (tag == Tags.PLAYER_TAG)
        {
            Invoke("RestartGame", 3f);
        }

        else
        {
            onEnemyKill();
            Invoke("TurnOffGameObject", 0f);
        }

    }

    void RestartGame()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("SampleScene");
    }

    void TurnOffGameObject()
    {
        gameObject.SetActive(false);
    }

    IEnumerator DeadSound()
    {
        yield return new WaitForSeconds(0.3f);
        //enemyAudio.Play_DeadSound();
    }

}
