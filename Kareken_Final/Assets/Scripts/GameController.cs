﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public static GameController gc;

    public GameObject player;
    public bool gameOver;
    public int socre = 0;

    // Start is called before the first frame update
    void Start()
    {
        if (gc = null)
        {
            gc = this;

            DontDestroyOnLoad(gameObject);
        }

        else
        {
            Destroy(gameObject);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
