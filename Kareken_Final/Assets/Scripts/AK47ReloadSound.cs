﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AK47ReloadSound : MonoBehaviour
{
    [SerializeField]
    private AudioSource audioSource;

    AudioClip temp;

    [SerializeField]
    private AudioClip[] magSounds;

    private static int index;


    private void Awake()
    {
        index = 0;

        //saves the shoot sound
        temp = audioSource.clip;
    }


    public void CycleSound()
    {
        if (index != 3)
        {
            audioSource.clip = magSounds[index];
            audioSource.Play();
            index++;
        }

        //last CycleSound resets audio clip to shoot sound
        else
        {
            index = 0;
            audioSource.clip = temp;
        }
    }

}
