﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum WeaponFireType
{
    SEMI,
    AUTOMATIC
}

public class WeaponHandler : MonoBehaviour
{
    private Animator anim;

    [SerializeField]
    private GameObject muzzleFlash;

    [SerializeField]
    private AudioSource shootSound, reloadSound;

    [SerializeField]
    public int maxClipSize;

    public int currentClipSize;

    public WeaponFireType fireType;

    private AK47ReloadSound ak47Reload;

    private void Awake()
    {
        anim = GetComponent<Animator>();
        ak47Reload = GetComponent<AK47ReloadSound>();

        currentClipSize = maxClipSize;
    }

    public void ShootAnimation()
    {
        anim.SetTrigger(AnimationTags.SHOOT_TRIGGER);
    }
    
    public void ReloadAnimation()
    {
        anim.SetTrigger(AnimationTags.RELOAD_TRIGGER);
    }

    void ToggleReloading()
    {
        if (PlayerShoot.reloading == true)
        {
            currentClipSize = maxClipSize;
            PlayerShoot.reloading = false;
        }

        else
        {
            PlayerShoot.reloading = true;
        }
    }

    void AK47ReloadAudio()
    {
        
        ak47Reload.CycleSound();
    }

    void Turn_On_MuzzleFlash()
    {
        muzzleFlash.SetActive(true);
    }

    void Turn_Off_MuzzleFlash()
    {
        muzzleFlash.SetActive(false);
    }

    void Play_ShootSound()
    {
        shootSound.Play();
    }

    void Play_ReloadSound()
    {
        reloadSound.Play();
    }
}
