﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShoot : MonoBehaviour
{
    private WeaponManager weaponManager;

    public float fireRate = 15f;
    public float damage = 20f;

    private float nextTimeToFire;

    private Camera mainCam;

    private GameObject crosshair;

    public static bool reloading;

    private void Awake()
    {
        weaponManager = GetComponent<WeaponManager>();

        //crosshair = GameObject.FindWithTag(Tags.CROSSHAIR);
        mainCam = Camera.main;
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (reloading == false)
        {
            WeaponShoot();
        }

        WeaponReload();
    }

    //check which weapon is being fired
    void WeaponShoot()
    {
        //check if weapon is empty
        if (weaponManager.GetCurrentSelectedWeapon().currentClipSize != 0)
        {
            //if weapon is an automatic type
            if (weaponManager.GetCurrentSelectedWeapon().fireType == WeaponFireType.AUTOMATIC)
            {
                //if left mouse button held down and Time > nextTimeToFire
                if (Input.GetMouseButton(0) && Time.time > nextTimeToFire)
                {
                    nextTimeToFire = Time.time + 1f / fireRate;
                    weaponManager.GetCurrentSelectedWeapon().ShootAnimation();

                    BulletFired();
                }
            }

            //if weapon is a semiautomatic type
            else
            {
                //if left mouse button held down
                if (Input.GetMouseButton(0))
                {
                    weaponManager.GetCurrentSelectedWeapon().ShootAnimation();

                    BulletFired();
                }
            }
        }
    }

    void BulletFired()
    {
        weaponManager.GetCurrentSelectedWeapon().currentClipSize--;

        RaycastHit hit;

        if (Physics.Raycast(mainCam.transform.position, mainCam.transform.forward, out hit))
        {
            if (hit.transform.tag == Tags.ENEMY_TAG)
            {
                hit.transform.GetComponent<Health>().ApplyDamage(damage * PlayerResize.currentSize);
            }
        }
    }

    void WeaponReload()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            weaponManager.GetCurrentSelectedWeapon().ReloadAnimation();
        }
    }
}
