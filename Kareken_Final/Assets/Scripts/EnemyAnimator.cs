﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAnimator : MonoBehaviour
{

    private Animator anim;

    // Start is called before the first frame update
    void Awake()
    {
        anim = GetComponent<Animator>();
    }

    public void Run(bool run)
    {
        anim.SetBool(AnimationTags.RUN_PARAMETER, run);
    }

    public void Shoot()
    {
        anim.SetTrigger(AnimationTags.ATTACK_PARAMETER);
    }
    
    public void Dead(bool dead)
    {
        anim.SetTrigger(AnimationTags.DEAD_TRIGGER);
    }
}
