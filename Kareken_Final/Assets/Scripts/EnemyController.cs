﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public enum EnemyState
{
    PATROL,
    CHASE,
    ATTACK
}

public class EnemyController : MonoBehaviour
{
    private EnemyAnimator enemyAnim;
    private NavMeshAgent navAgent;
    private GameObject player;

    private EnemyState enemyState;

    public float walkSpeed = 0.5f;
    public float runSpeed = 4f;

    public float chaseDistance = 7f;
    private float currentChaseDistance;
    public float attackDistance = 1.8f;
    public float chaseAfterAttackDistance = 2f;

    public float patrolRadiusMin = 20f, patrolRadiusMax = 60f;
    public float patrolForThisTime = 15f;
    private float patrolTimer;

    public float waitBeforeAttack = 2f;
    private float attackTimer;

    private Transform target;

    public GameObject attackPoint;

    //private EnemyAudio enemy_Audio;

    void Awake()
    {
        enemyAnim = GetComponent<EnemyAnimator>();
        navAgent = GetComponent<NavMeshAgent>();

        target = GameObject.FindWithTag(Tags.PLAYER_TAG).transform;
        player = GameObject.FindWithTag(Tags.PLAYER_TAG);

        //enemy_Audio = GetComponentInChildren<EnemyAudio>();

    }

    // Use this for initialization
    void Start()
    {

        enemyState = EnemyState.PATROL;

        patrolTimer = patrolForThisTime;

        // when the enemy first gets to the player
        // attack right away
        attackTimer = waitBeforeAttack;

        // memorize the value of chase distance
        // so that we can put it back
        currentChaseDistance = chaseDistance;

    }

    // Update is called once per frame
    void Update()
    {

        if (enemyState == EnemyState.PATROL)
        {
            Patrol();
        }

        if (enemyState == EnemyState.CHASE)
        {
            Chase();
        }

        if (enemyState == EnemyState.ATTACK)
        {
            Attack();
        }

    }

    void Patrol()
    {
        navAgent.isStopped = false;
        navAgent.speed = walkSpeed;

        patrolTimer += Time.deltaTime;

        if (patrolTimer > patrolForThisTime)
        {
            SetNewRandomDestination();
            patrolTimer = 0f;
        }

        if (navAgent.velocity.sqrMagnitude > 0)
        {
            enemyAnim.Run(true);
        }

        else
        {
            enemyAnim.Run(false);
        }

        if (Vector3.Distance(transform.position, target.position) <= chaseDistance)
        {
            enemyState = EnemyState.CHASE;

            //play audio?
        }
    }

    void Chase()
    {
        navAgent.isStopped = false;
        navAgent.speed = runSpeed;
        navAgent.SetDestination(target.position);

        if (navAgent.velocity.sqrMagnitude > 0)
        {
            enemyAnim.Run(true);
        }

        else
        {
            enemyAnim.Run(false);
        }

        if (Vector3.Distance(transform.position, target.position) <= attackDistance)
        {
            enemyAnim.Run(false);

            enemyState = EnemyState.ATTACK;

            //play audio?

            if (chaseDistance != currentChaseDistance)
            {
                     chaseDistance = currentChaseDistance;
            }
        }

        //if player runs away from enemy
        else if (Vector3.Distance(transform.position, target.position) > chaseDistance)
        {
            enemyAnim.Run(false);
            enemyState = EnemyState.PATROL;

            patrolTimer = patrolForThisTime;

            if (chaseDistance != currentChaseDistance)
            {
                chaseDistance = currentChaseDistance;
            }
        }
    }

    void Attack()
    {
        navAgent.velocity = Vector3.zero;
        navAgent.isStopped = true;

        attackTimer += Time.deltaTime;

        Vector3 targetPostition = new Vector3(target.position.x, this.transform.position.y, target.position.z);
        navAgent.transform.LookAt(targetPostition);

        if (attackTimer > waitBeforeAttack)
        {
            enemyAnim.Shoot();
            attackTimer = 0f;

            //enemy fires a bullet at player
            RaycastHit hit;

            if (Physics.Raycast(attackPoint.transform.position, attackPoint.transform.forward, out hit))
            {
                if (hit.transform.tag == Tags.PLAYER_TAG)
                {
                    hit.transform.GetComponent<Health>().ApplyDamage(20);
               }
            }

            //temporary damage mechanic
            System.Random randInt = new System.Random();
            if (randInt.Next(10) < 4)
            {
                player.GetComponent<Health>().ApplyDamage(20);
            }
        }

        if (Vector3.Distance(transform.position, target.position) > attackDistance + chaseAfterAttackDistance)
        {
            enemyState = EnemyState.CHASE;
        }
    }

    void SetNewRandomDestination()
    {
        float randRadius = Random.Range(patrolRadiusMin, patrolRadiusMax);
        Vector3 randDir = Random.insideUnitSphere * randRadius;
        randDir += transform.position;

        NavMeshHit navHit;

        NavMesh.SamplePosition(randDir, out navHit, randRadius, -1);
        navAgent.SetDestination(navHit.position);

    }

    void TurnOnAttackPoint()
    {

    }

    void TurnOffAttackPoint()
    {

    }

    public EnemyState Enemy_State
    {
        get; set;
    }

}