﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoBehaviour
{

    public static EnemyManager instance;

    [SerializeField]
    //private GameObject ;

    public Transform[] enemySpawnPoints;

    [SerializeField]
    private int enemyCount;

    public float spawnWaitTimer = 10f;

    // Use this for initialization
    void Awake()
    {
        MakeInstance();
    }

    void Start()
    {
        
    }

    void MakeInstance()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    void SpawnEnemies()
    {
        
    }

    IEnumerator CheckToSpawnEnemies()
    {
        yield return new WaitForSeconds(spawnWaitTimer);

        StartCoroutine("CheckToSpawnEnemies");
    }

    public void EnemyDied(bool enemy)
    {

    }

    public void StopSpawning()
    {
        StopCoroutine("CheckToSpawnEnemies");
    }

}